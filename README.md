### 描述

收集一些自用的一键安装脚本

### 用法

```sh
# g 取义:
# 增值的G 快速复制
# get
# 例子: 快速安装 docker 并设置开机自启
curl https://g.wsl.fun/docker/install | sh
```

#### docker

```sh
# CHANNEL: 接受更新的通道. 可能值: `stable`(稳定版) `edge`(测试版)
# VERSION: 版本号.
# --mirror 镜像地址. 可能值: `Aliyun` `AzureChinaCloud` `(空)`
# --registry_mirror docker 镜像加速地址: 默认是 ustc 的源, 这个需要使用了 --mirror 或者 --registry_mirror 才会开启 
curl -sSL https://g.wsl.fun/docker/install | VERSION="18.06.0-ce" CHANNEL="stable" sh -s -- --mirror Aliyun --registry_mirror https://registry.docker-cn.com/
# 上面那两个值是默认值
# 默认不使用 Aliyun 镜像加速
curl -sSL https://g.wsl.fun/docker/install | sh -s -- --mirror Aliyun --registry_mirror https://registry.docker-cn.com/
```

### nodejs
```sh
# 安装 npm 
curl -sSL https://g.wsl.fun/nodejs/install | VERSION=10.14.2 sh
# 上面的是默认值
curl -sSL https://g.wsl.fun/nodejs/install | sh
```